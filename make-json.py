#!/usr/bin/env python
import csv, json, os, urlparse
from tld import get_tld

import search_engines

'''
In late March 2013 I put together a terrible set of scripts for remotely managing ongoing renders on my home computer, and one of them sends one HTTP request to an unlisted URL on our webserver every ten seconds. I forgot about it and left it running 24/7 until January 2016. Because it never touched anything Google could possibly know or care about, and even though it sent around 200,000 requests every month adding up to a total of 6,446,776 requests over nearly three years, I honestly believe it is not relevant in the discussion of our AdSense account's suspension for detected invalid activity and would only distract from the visualization of the traffic that did register in Google's analytics.
'''
render_ips = ['98.234.4.242', '24.4.124.97', '73.15.169.60']
render_total = 0

def read_map(awsfile):
	tokens = awsfile.readline().split()
	while len(tokens) != 2 or tokens[0] != 'BEGIN_MAP':
		tokens = awsfile.readline().split()
	entry_count = int(tokens[1])

	entries = {}
	line = awsfile.readline()
	while line != 'END_MAP\n':
		tokens = line.split()
		entries[tokens[0]] = int(tokens[1])
		line = awsfile.readline()
	return entries

# Takes in a string of a line and splits it by space, reading all tokens but the first as ints
def split_entry(line, max_count):
	return [(val if i == 0 else int(val)) for (i, val) in enumerate(line.split()[:max_count])]

def read_section(awsfile, byte_pos, max_count=99):
	awsfile.seek(byte_pos)
	entry_count = split_entry(awsfile.readline(), 2)[1]
	entries = []
	for i in xrange(entry_count):
		entries.append(split_entry(awsfile.readline(), max_count))
	return entries

def add_url_to_domains(url, count, is_se, domains):
	try:
		domain = get_tld(url)
	except:
		try:
			domain = urlparse.urlparse(url).hostname
		except:
			print('BAD URL. I HONESTLY DOUBT THAT %i PEOPLE CAME FROM %s' % (count, url))
			return
	if domain not in domains:
		domains[domain] = {'count':0, 'pages':{}, 'se':False}
	if is_se:
		domains[domain]['se'] = True
	domains[domain]['count'] += count

	pageid = '@'.join(url.split(domain, 1))
	if pageid not in domains[domain]['pages']:
		domains[domain]['pages'][pageid] = 0
	domains[domain]['pages'][pageid] += count

min_views  = 80  # Minimum number of pageviews from 1 IP address to land on our creep list
min_total_refers = 500 # Minimum number of pageviews from 1 referer to land on our VIP list
min_page_refers = 50

first_month = 5
first_year  = 2012
last_month   = 4
last_year	= 2016

general_data = {'months': []}
with open('events.json') as events_file:
	general_data['events'] = json.load(events_file)

unfound_se = []

for year in range(first_year, last_year + 1):
	this_first_month = first_month if year == first_year else 1
	this_last_month  =  last_month if year ==  last_year else 12
	for month in range(this_first_month, this_last_month + 1):
		month_id = '%04d-%02d' % (year, month)
		print('Now doing %s' % month_id)
		aws_filename = 'awstats/awstats%02d%04d.htwins.net.txt' % (month, year)

		with open(aws_filename) as awsfile:
			entries = read_map(awsfile)
			month_general = {}

			# === DAILY PAGE VIEWS ===

			day_entries = read_section(awsfile, entries['POS_DAY'])
			print('  There were %i days!' % len(day_entries))
			start_day = day_entries[0][0][-2:]
			month_general['s'] = int(start_day) - 1
			month_general['d'] = [entry[1] for entry in day_entries]
			month_general['t'] = sum(month_general['d'])

			# === HOURLY PAGE VIEWS ===
			hour_entries = read_section(awsfile, entries['POS_TIME'])
			print('  And %i hours. Huh.' % len(hour_entries))
			month_general['h'] = [entry[1] for entry in hour_entries]
			#general_data['hours'].append(

			# === IP ADDRESSES ===

			ip_entries = read_section(awsfile, entries['POS_VISITOR'], 2)
			print('  We had %i blessed visitors!' % len(ip_entries))
			ips = [entry[:2] for entry in ip_entries if entry[1] >= min_views and entry[0] not in render_ips]
			ips.sort(key=lambda entry: entry[1], reverse=True)
                        ips = ips[:100]

			with open('data/ip-%s.csv' % month_id, 'w') as ip_file:
				writer = csv.writer(ip_file)
				writer.writerow(['ip', 'count'])
				writer.writerows(ips)

			general_data['months'].append(month_general)

			# === REFERERS ===

			referer_entries = read_section(awsfile, entries['POS_PAGEREFS'])
			print('  From an amazing %i referers!' % len(referer_entries))
			domains = {}
			for referer in referer_entries:
				url   = referer[0]
				count = referer[1]
				add_url_to_domains(url, count, False, domains)

			# === SEARCH ENGINE REFERERS ===

			engine_entries = read_section(awsfile, entries['POS_SEREFERRALS'])
			print('  Also %i search engines that would\'ve been easier to deal with if they were listed by domain and not some hash' % len(engine_entries))
			for engine_entry in engine_entries:
				if engine_entry[0] == 'search' or engine_entry[1] < min_page_refers:
					continue
				if engine_entry[0] not in search_engines.engines:
					print('WHAT A WEIRD SEARCH ENGINE! WHAT EVEN IS %s?' % engine_entry[0])
					unfound_se.append(engine_entry[0])
					continue
				url   = search_engines.engines[engine_entry[0]]
				count = engine_entry[1]
				add_url_to_domains(url, count, True, domains)

			domains_write = [[entry[0], {'c':entry[1]['count'], 'se':entry[1]['se'], 'p':[page for page in sorted(entry[1]['pages'].items(), key=lambda page: page[1], reverse=True) if page[1] >= min_page_refers]}] for entry in domains.items() if entry[1]['count'] >= min_total_refers]
			domains_write.sort(key=lambda entry: entry[1]['c'], reverse=True)

			with open('data/refer-%s.json' % month_id, 'w') as refer_file:
				json.dump(domains_write, refer_file, separators=(',', ':'))
print('Unfound major search engines:')
print(unfound_se)

print('Writing general data JSON')
with open('data/general.json', 'w') as general_file:
	json.dump(general_data, general_file, separators=(',', ':'))

