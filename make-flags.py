#!/usr/bin/env python
from __future__ import print_function
import os, subprocess, shutil, tempfile, xmltodict
import unicodecsv as csv

# You probably want to change this
flag_width  = 20
flag_height = 15
flag_padding = 1

sheet_width = 3
in_flag_dir    = 'flags'
out_sheet_path = 'flags.png'
out_css_path   = 'flags.css'
found_countries = []

with open('geoip.csv') as geoip_file:
    reader = csv.DictReader(geoip_file)
    for row in reader:
        if row['countrycode'] and row['countrycode'] not in found_countries:
            found_countries.append(row['countrycode'])
found_countries = sorted([code.lower() for code in found_countries])

print('Countries:')
print(found_countries)

def flag_filename(country_code):
    return os.path.join(in_flag_dir, country_code + '.svg')

print('Writing flag spritesheet to %s' % out_sheet_path)
subprocess.call(
        ['montage'] +
        [flag_filename('zz') if not os.path.isfile(flag_filename(country_code)) else flag_filename(country_code) for country_code in found_countries] +
        ['-resize', '%ix%i!' % (flag_width * 2, flag_height * 2),
        '-geometry', '+%i+%i' % (flag_padding * 2, flag_padding * 2),
        '-tile', '%ix' % sheet_width,
        out_sheet_path]
)
print('Optimizing %s' % out_sheet_path)
subprocess.call([
    'optipng',
    '-o7',
    out_sheet_path
])

print('Writing CSS to %s' % out_css_path)
with open(out_css_path, 'w') as css_file:
    css_file.write('.flag {\n')
    css_file.write('\tdisplay: inline-block;\n')
    css_file.write('\tmargin-right: 5px;\n')
    css_file.write('\tvertical-align: text-bottom;\n')
    css_file.write('\twidth:  %ipx;\n' % flag_width)
    css_file.write('\theight: %ipx;\n' % flag_height)
    css_file.write('\tbackground: url("%s");\n' % out_sheet_path)
    css_file.write('\tbackground-size: %ipx;\n' % (sheet_width * (flag_width + flag_padding * 2)))
    css_file.write('}\n')
    for i, country in enumerate(found_countries):
        flag_x = (i % sheet_width) * (flag_width  + flag_padding * 2) + flag_padding
        flag_y = (i / sheet_width) * (flag_height + flag_padding * 2) + flag_padding
        css_file.write('.flag-%s{' % country)
        css_file.write('background-position:%ipx %ipx;' % (-flag_x, -flag_y))
        css_file.write('}')
