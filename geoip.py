#!/usr/bin/env python
import os, urllib2, xmltodict
import unicodecsv as csv
from tld import get_tld

csv_paths = ['data/' + path for path in os.listdir('data') if path[:3] == 'ip-']
ip_dict = {}

print('Reading CSV files...')

for csv_path in csv_paths:
    with open(csv_path) as csv_file:
        reader = csv.reader(csv_file)
        for row in reader:
            ip_dict[row[0]] = True

count = len(ip_dict)
print('There were %i unique IPs.' % count)

index = 0
with open('data/geoip.csv', 'w') as csvfile:
    writer = csv.DictWriter(csvfile, fieldnames=['ip', 'isp', 'city', 'countrycode', 'latitude', 'longitude'], extrasaction='ignore', encoding='utf-8')
    writer.writeheader()
    for key in ip_dict:
        if index < 0: # Modify for restarting at a certain point
            index += 1
            continue
        url = 'http://api.geoiplookup.net/?query=' + key
        try:
            xmlfile = urllib2.urlopen(url)
            xml = xmlfile.read().replace('&', '&amp;')
            print(xml)
            index += 1
            print('Loaded %i of %i' % (index, count))
            xmlfile.close()

            data = xmltodict.parse(xml)
            print(data['results']['result'])
            writer.writerow(data['results']['result'])
        except urllib2.URLError, err:
            print(err.reason)
        finally:
                try:
                    xmlfile.close()
                except NameError:
                    pass
