Traffic.HourWidget = function(container) {
	this.svg = container.select('svg');
	this.size = this.svg.node().getBoundingClientRect();
	this.svg.style('height', '200px');
	this.size = this.svg.node().getBoundingClientRect();

	var margin = {top: 30, right: 30, bottom: 30, left: 50};
	this.graphWidth  = this.size.width  - margin.left - margin.right;
	this.graphHeight = this.size.height - margin.top  - margin.bottom;
	this.xScale  = d3.scale.linear()
		.domain([0, 24])
		.range([0, this.graphWidth]);
	this.yScale = d3.scale.linear()
		.domain([0, 50000])
		.range([this.graphHeight, 0]);
	this.xAxis = d3.svg.axis()
		.scale(this.xScale)
		.orient('bottom')
		.tickValues(d3.range(0, 25, 3))
		.tickSize(10)
		.tickFormat(function(d) { return d3.format('02')(d) });
	this.yAxis = d3.svg.axis()
		.scale(this.yScale)
		.orient('left')
		.tickSize(10)
		.ticks(6)
		.tickFormat(d3.format('s'));
	var chart = this.svg.append('g')
		.attr('transform', 'translate(' + margin.left + ',' + margin.top + ')');
	chart.append('g')
		.attr('class', 'x axis')
		.attr('transform', 'translate(0,' + this.graphHeight + ')')
		.call(this.xAxis);
	chart.append('g')
		.attr('class', 'y axis')
		.call(this.yAxis);
	this.barG = chart.append('g');
	this.noonMarker = chart.append('g')
		.attr('class', 'noon-marker')
		.attr('transform', 'translate(' + (this.graphWidth + 20) + ')');
	this.noonMarker.append('line')
		.attr('y1', -10)
		.attr('y2', this.graphHeight);
	this.noonMarker.append('text')
		.text('Local noon')
		.style('text-anchor', 'middle')
		.attr('y', -15);
}

Traffic.HourWidget.prototype.update = function(hours, date) {
	this.yScale.domain([0, d3.max(hours)]);

	this.svg.select('.y.axis')
		.transition()
		.duration(1000)
		.call(this.yAxis);

	var hourBars = this.barG.selectAll('rect').data(hours);
	var that = this;
	hourBars.enter().append('rect')
		.attr('x', function(d, i) { return that.xScale(i) })
		.attr('y', that.graphHeight)
		.attr('width', that.xScale(1) - that.xScale(0))
		.attr('height', 0);
	hourBars.transition()
		.duration(1000)
		.attr('y', function(count) { return that.yScale(count) })
		.attr('height', function(count) {
			return that.graphHeight - that.yScale(count);
		});

	var noonX = that.xScale(date.getTimezoneOffset() / 60);
	this.noonMarker.transition()
		.duration(1000)
		.attr('transform', 'translate(' + noonX + ')');
}
