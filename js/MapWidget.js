(function() {
	Traffic.MapWidget = function(container, leafletID) {
		this.container = container;

		var mapboxToken = 'pk.eyJ1IjoibWljaGFlbHlodWFuZyIsImEiOiJjaW45amFuNWUwMGxodWZsd2ZscnVrMWl0In0.CFmKxOJ65Oo0MEYAjSEWQw';

		var mapboxTiles = L.tileLayer('https://api.mapbox.com/styles/v1/michaelyhuang/cin9jlvzr0021b4no6hm6vjlh/tiles/{z}/{x}/{y}' + (L.Browser.retina ? '@2x': '') + '?access_token=' + mapboxToken, {
			detectRetina: false,
			tileSize: 512,
			zoomOffset: -1,
			minZoom: 1
		});
		this.map = new L.Map(leafletID, {center: [40, 0], zoom: 1}).addLayer(mapboxTiles)

		this.geoipLoaded = false;
		this.geoip = {};
		this.unmarkeredHosts = [];
	}

	Traffic.MapWidget.prototype.addHosts = function(geoipHosts) {
		this.geoipLoaded = true;
		geoipHosts.forEach(function(host) {
			this.geoip[host.ip] = host;
		}.bind(this));

		this.updateRows(this.unmarkeredHosts);
		this.updateMarkers(this.unmarkeredHosts);
		this.unmarkeredHosts = [];
	}

	function setHostHighlight(datum, isHighlighted) {
		d3.select(datum['tableRow']).classed('highlight', isHighlighted);
		d3.select(datum['bubble'  ]).classed('highlight', isHighlighted);
	}
	Traffic.MapWidget.prototype.selectHost = function(host) {
		var geo = this.geoip[host['ip']];
		if (!geo || !geo['latitude']) return;
		//that.map.setView([geo['latitude'], geo['longitude']], 9, {animate: true});
		var bubbleContent = '<pre>' +
			host['ip'] + '\n' +
			'PAGEVIEWS: ' + Traffic.thousands(host['count']) + '\n' +
			'--------------\n' +
			'ISP:  ' + (geo['isp'] ? geo['isp'] : 'Unknown') + '\n' +
			'CITY: ' + (geo['city'] ? geo['city'] : 'Unknown') + ' - ' + geo['countrycode'] +
			'</pre>';
		L.popup()
			.setLatLng([geo['latitude'], geo['longitude']])
			.setContent(bubbleContent)
			.openOn(this.map);
	};

	Traffic.MapWidget.prototype.updateRows = function(hosts) {
		var that = this;
		var tbody = this.container.select('tbody');
		var rows = tbody.selectAll('tr').data(hosts/*, function(d) { return d.ip }*/);

		rows.enter().append('tr');
		rows.each(function(d) { d['tableRow'] = this })
			.on('mouseover', function(d) { setHostHighlight(d, true ) })
			.on('mouseout',  function(d) { setHostHighlight(d, false) })
			.on('click', this.selectHost.bind(this));

		var cells = rows.selectAll('td')
			.data(function(d) {
				var geo = that.geoip[d.ip];
				var flagClass;
				if (geo && geo['countrycode']) {
					flagClass = 'flag flag-' + geo['countrycode'].toLowerCase();
				} else {
					flagClass = 'flag flag-xk';
				}
				return [
					{col: 'ip',    val: [flagClass, d.ip]},
					{col: 'count', val: d.count}
				];
			});
		cells.enter().append('td');
		cells.each(function(d) {
			var cell = d3.select(this);
			if (d.col == 'count') {
				cell.text(Traffic.thousands(d.val));
			} else {
				var spans = cell.selectAll('span').data(d.val);
				spans.enter().append('span');
				spans
					.attr('class', function(d, i) { return i == 0 ? d : '' })
					.text(         function(d, i) { return i == 0 ? '' : d });
			}
		});
		cells.exit().remove();
		rows.exit().remove();

	};

	Traffic.MapWidget.prototype.updateMarkers = function(hosts) {
		var that = this;

		var iclon = L.divIcon({className: 'iclon'});
		var mapViewsScale = d3.scale.sqrt()
			.domain([0, 10000])
			.range([.05, 2]);

		var markers = d3.select('.leaflet-marker-pane').selectAll('.iclon')
			.data(hosts, function(host) { return host.ip });
		markers.enter().call(function(unmarkeredData) {
			unmarkeredData[0].forEach(function(host) { // Custom "append" by creating Leaflet DivIcon markers
				if (!host.__data__) return;
				var geo = that.geoip[host.__data__.ip];
				if (!geo || !geo['latitude']) return;

				var marker = new L.marker([geo['latitude'], geo['longitude']], {icon: iclon});
				marker.on('click', function() {
					that.selectHost(this._icon.__data__);
				});
				marker.addTo(that.map);
				d3.select(marker._icon)
					.datum(host.__data__)
					.on('mouseover', function(d) { setHostHighlight(d, true ) })
					.on('mouseout',  function(d) { setHostHighlight(d, false) })
					.append('div')
					.attr('class', 'existent')
					.style('transform', 'translateZ(0) scale(0)');

				marker._icon['marker'] = marker;
			});
		});
		markers.exit()
			.each(function(host) {
				var marker = this['marker'];
				d3.select(this).select('div')
					.attr('class', '')
					.style('transform', 'translateZ(0) scale(0)')
					.transition()
					.duration(1000)
					.each('end', function() {
						that.map.removeLayer(marker);
					});
			});
		markers = d3.select('.leaflet-marker-pane').selectAll('.iclon')
			.each(function(host) {
				host['bubble'] = this;
			});
		markers.select('.existent')
			.transition()
			.duration(0)
			.each('end', function(host) {
				d3.select(this).style('transform', 'translateZ(0) scale(' + mapViewsScale(host.count) + ')');
			});
	};

	Traffic.MapWidget.prototype.update = function(hosts) {
		this.updateRows(hosts);

		var mapHosts = hosts.splice(0, 100);
		if (this.geoipLoaded) {
			this.updateMarkers(mapHosts);
		} else {
			this.unmarkeredHosts = mapHosts;
		}
	};
})();
