
var sidebar = AJS.sidebar('.aui-sidebar');

// All of this to disable sidebar collapsing
// (kbd shortcut '[', initial & resized browser width)
if (sidebar.isCollapsed()) sidebar.expand();
sidebar.$el.removeClass('aui-sidebar-fly-out');
sidebar.on('collapse-start', function(e) { e.preventDefault(); });

var mapWidget = new Traffic.MapWidget(d3.select('#ip'), 'leaflet-container');

d3.csv('geoip.csv', function(error, hosts) {
	if (error) throw error;
	mapWidget.addHosts(hosts);
});

var graphNav = d3.select('#graph-nav')
d3.json('data/general.json', function(error, generalData) {
	if (error) throw error;

	var graphWidth = 279;
	var monthLabelWidth = 25;

	var daysOverMilliseconds = 1 / 1000 / 60 / 60 / 24;

	var limits = [new Date(2012, 4, 1), new Date(2016, 4, 31)];
	var x = d3.scale.linear()
		.domain([0, d3.max(generalData['months'].map(function(d) { return d3.max(d.d) }))])
		.range([monthLabelWidth, graphWidth])
		.nice();
	var y = d3.time.scale()
		.domain(limits)
		.range([0, (limits[1] - limits[0]) * daysOverMilliseconds * 2.0]);

	var graphAxis = d3.svg.axis()
		.scale(x)
		.orient('bottom')
		.ticks(3)
		.tickSize(10)
		.tickFormat(d3.format('s'))
	d3.select('#graph-scale')
		.append('g')
		.call(graphAxis)

	// === MONTHLY GRAPHS ===

	var justMonth = d3.time.format('%b');
	var justYear  = d3.time.format('%Y');
	var shortDate = d3.time.format('%-m/%-d');
	// An array of booleans corresponding to months: does this month connect with the next?
	var monthsConnect = generalData['months'].map(function(monthData, i) {
		// False if it's the last month
		if (i == generalData['months'].length - 1) return false;
		// False if the next month doesn't start on the 1st
		if (generalData['months'][i + 1].s != 0) return false;

		var month     = d3.time.month.offset(limits[0], i);
		var nextMonth = d3.time.month.offset(month,     1);
		// False if there aren't enough days to reach the next month
		if (d3.time.day.offset(month, monthData.s + monthData.d.length) >= nextMonth) {
			monthData.d.push(generalData['months'][i + 1].d[0]);
			return true;
		}
		return false;
	});
	var monthSvgs = graphNav.selectAll('svg.month')
		.data(generalData['months'])
		.enter()
		.append('svg')
		.attr('class', 'month')
		.on('click', function(d, i) { loadMonth(d3.time.month.offset(limits[0], i)) })
		.each(function(monthData, i) {
			var month = d3.time.month.offset(limits[0], i);
			var topY = y(month);
			var height = y(d3.time.month.offset(month, 1)) - topY;
			var monthSvg = d3.select(this)
				.style('top', topY + 'px' )
				.style('height', height + 'px' );
			monthSvg.append('rect')
				.attr('class', 'background')
				.attr('y', 1)
				.attr('height', height - 2);

			if (monthsConnect[i]) {
				var nextMonth = generalData['months'][i + 1];
				monthData.d.push(nextMonth.d[0]);
			}
			monthData.d.push(monthData.d[monthData.d.length - 1]);

			var lastX = x(monthData.d[monthData.d.length - 1]);

			var shiftedX = function(d) { return x(d) - lastX };
			monthData.yScale = d3.scale.linear()
				.domain([0, monthData.d.length])
				.range([monthData.s * 2, (monthData.s + monthData.d.length) * 2]);
			var monthArea = d3.svg.area()
				.x(shiftedX)
				.x0(-graphWidth)
				.y(function(d, i) { return i == monthData.d.length - 1 ? 500 : monthData.yScale(i)});
			monthSvg.append('path')
				.attr('class', 'area')
				.datum(monthData.d)
				.attr('d', monthArea)
				.attr('transform', 'translate(' + lastX + ',0)');
			var monthLine = d3.svg.line()
				.x(shiftedX)
				.y(function(d, i) { return i == monthData.d.length - 1 ? 500 : monthData.yScale(i)});
			monthSvg.append('path')
				.attr('class', 'line')
				.datum(monthData.d)
				.attr('d', monthLine)
				.attr('transform', 'translate(' + lastX + ',0)');

			monthSvg.append('rect')
				.attr('class', 'baseline-domain');
			monthSvg.append('rect')
				.attr('class', 'baseline-views');
			monthSvg.append('rect')
				.attr('class', 'baseline');
			monthSvg.selectAll('rect')
				.attr('x', -5)
				.attr('y', -5)
				.attr('width', monthLabelWidth + 5)
				.attr('height', height + 10);
			monthSvg.append('text')
				.text(justMonth(month))
				.attr('transform', 'translate(7,' + (height * .5) + ') rotate(90)');
		}).on('mousemove', function(monthData, i) {
			var mouseY = d3.mouse(this)[1];
			var dayIndex = Math.round(monthData.yScale.invert(mouseY));
			var date = d3.time.day.offset(d3.time.month.offset(limits[0], i), monthData.s + dayIndex);
			d3.select(this).select('text').text(shortDate(date));
			if (dayIndex < 0 || dayIndex >= monthData.d.length) {
				d3.select('#tooltip').style('display', 'none');
				return;
			}
			var dayValue = monthData.d[dayIndex]
			var angle = 0;
			if (dayIndex > 0 && dayIndex < monthData.d.length - 1) {
				angle = Math.atan2(monthData.yScale(dayIndex + 1) - monthData.yScale(dayIndex - 1), x(monthData.d[dayIndex + 1]) - x(monthData.d[dayIndex - 1]));
				angle = angle * 180 / Math.PI - 90;
				angle *= .1;
			}
			var farRight = x(dayValue) > graphWidth - 60;
			angle = farRight ? angle + 180 : angle;
			var textAngle = farRight ? 180 : 0;
			d3.select('#tooltip')
				.style('display', 'block')
				.style('top', d3.select(this).style('top'))
				.style('transform', 'translate3d(' + (x(dayValue) + 20) + 'px, ' + (monthData.yScale(dayIndex) - 13) + 'px,0)rotate(' + angle + 'deg)')
				.html(farRight ? '<span>' + Traffic.thousands(dayValue) + '</span>' : Traffic.thousands(dayValue));
		}).on('mouseout', function(monthData, i) {
			var month = d3.time.month.offset(limits[0], i);
			d3.select('#tooltip').style('display', 'none');
			d3.select(this).select('text').text(justMonth(month));
		});

	var monthDividers = graphNav.selectAll('div.ider')
		.data(d3.time.months(limits[0], limits[1]))
		.enter()
		.append('div')
		.attr('class', 'ider')
		.style('top', function(d, i) { return y(d3.time.month.offset(limits[0], i + 1)) - 1 + 'px' })
		.each(function(month) {
			var divider = d3.select(this);
			if (month.getMonth() == 11) {
				divider.append('span').text(+justYear(month) + 1);
				divider.append('div');
				divider.append('div');
			} else {
				divider.append('div');
			}
		});

	function getMonthIndex(date) {
		return d3.time.months(limits[0], date).length;
	}

	graphNav.selectAll('div.event')
		.data(generalData['events'])
		.enter()
		.append('div')
		.attr('class', 'event')
		.each(function(d, i) {
			var eventDate = new Date(d['date']);
			var monthIndex = getMonthIndex(eventDate);
			var dayIndex = eventDate.getDate();
			var div = d3.select(this)
				.style('top', y(eventDate) + 'px')
				.style('left', monthLabelWidth + 'px')
				.style('width', (graphWidth - monthLabelWidth) + 'px');
			if (d['url']) {
				div.append('a')
					.attr('href', d['url'])
					.attr('target', '_blank')
					.text(d['title']);
			} else {
				div.text(d['title']);
			}
		});

	graphNav.style('height', y.range()[1] + 'px');

	// Refer globals
	
	var domainSelected = false;
	var selectedDomain = '';
	var typeTimeout = -1;

	var navDomain = d3.select('#nav-domain')
		.on('click', function() {
			if (!domainSelected) graphDomain(selectedDomain);
		})
		.on('input', function() {
			clearTimeout(typeTimeout);
			var typedValue = this.value;
			if (!typedValue || typedValue.indexOf('.') == -1 || typedValue == selectedDomain) {
				return;
			}
			typeTimeout = setTimeout(function() {
				graphDomain(typedValue);
			}, 400);
		});
	d3.select('#nav-views').on('click', function() { ungraphDomain() });

	var bubbleWidget = new Traffic.BubbleWidget(d3.select('#refer'), function(r) {
		if (domainSelected && r == selectedDomain) {
			ungraphDomain();
		} else {
			var currentValue = domainSelected ? navDomain.node().value : ' ';
			navDomain.transition().duration(domainSelected ? currentValue.length * 30 : 300)
				.ease('quad-in')
				.attrTween('notvalue', function(d) {
					return function(t) {
						var len = Math.floor(currentValue.length * t);
						navDomain.node().value = currentValue.substring(len);
					}
				})
				.transition().duration(r.length * 50)
				.ease('quad-out')
				.attrTween('notvalue', function(d) {
					return function(t) {
						var len = Math.ceil(r.length * t);
						navDomain.node().value = r.substring(0, len);
					}
				});
			graphDomain(r);
		}
	});

	var hourWidget = new Traffic.HourWidget(d3.select('#hour'));

	function loadMonth(date) {
		var needToReselectBubble = domainSelected;
		if (needToReselectBubble) {
			bubbleWidget.selectReferer('');
		}

		var monthIndex = getMonthIndex(date);

		// Update header & buttons

		var titleFormat = d3.time.format('%B %Y');
		d3.select('.month-title')
			.text(titleFormat(date));
		d3.select('#month-header > a:first-child')
			.on('click', function() { loadMonth(d3.time.month.offset(date, -1)) });
		d3.select('#month-header > a:last-child')
			.on('click', function() { loadMonth(d3.time.month.offset(date,  1)) });

		monthSvgs    .classed('selected', function(d, i) { return i == monthIndex });
		monthDividers.classed('selected', function(d, i) { return i == monthIndex || i == monthIndex - 1 });

		// Update hourly views

		var hours = generalData['months'][monthIndex]['h'];
		hourWidget.update(hours, date);

		// Load specific data`

		var monthId = d3.time.format('%Y-%m')(date);
		d3.csv ('data/ip-'    + monthId +  '.csv', function(error, data) {
			if (error) throw error;
			mapWidget.update(data);
		});
		d3.json('data/refer-' + monthId + '.json', function(error, data) {
			if (error) throw error;
			bubbleWidget.update(data);
			if (needToReselectBubble) {
				bubbleWidget.selectReferer(selectedDomain);
			}
		});
	}

	loadMonth(new Date(2012, 5, 1));

	function graphDomain(domain) {
		bubbleWidget.selectReferer(domain);
		d3.select('.aui-sidebar').classed('domain', true);
		d3.select('#dotz').classed('domain', true);

		selectedDomain = domain;
		domainSelected = true;

		var idFormat = d3.time.format('data/refer-%Y-%m.json');
		var q = d3_queue.queue();
		monthSvgs.each(function(monthData, i) {
			var month = d3.time.month.offset(limits[0], i);
			q.defer(d3.json, idFormat(month));
		});
		q.awaitAll(function(error, referMonths) {
			if (error) throw error;

			var refererMonths = referMonths.map(function(monthReferers) {
				return monthReferers.find(function(d) { return d[0] == domain });
			});
			// Used only for cursor label
			var newBindings = refererMonths.map(function(refererMonth) {
				return {
					's': 0,
					'd': [refererMonth ? refererMonth[1]['c'] : 0],
					'yScale': d3.scale.linear().domain([0, 1]).range([15 * 2, 15 * 2])
				};
			});
			var newX = x.copy()
				.domain([0, d3.max(newBindings, function(d) { return d['d'][0] })])
				.nice();

			monthSvgs.data(newBindings).each(function(d, i) {
				var h = this.getBoundingClientRect().height;
				var monthSvg = d3.select(this);
				monthSvg.selectAll('path')
					.transition()
					.duration(500)
					.delay(i * 30)
					.attr('transform', 'translate(' + newX(d['d'][0]) + ',' + (-h) + ')');
			});

			var icon = d3.select('#icon');
			icon.selectAll('image')
				.transition().duration(1000)
				.attr('transform', 'translate(20,20)')
				.remove();
			icon.append('image')
				.attr('width', 16)
				.attr('height', 16)
				.attr('xlink:href', 'http://' + domain + '/favicon.ico')
				.attr('transform', 'translate(-16,-16)')
				.transition().duration(1000)
				.attr('transform', 'translate(0,0)');

			graphAxis.scale(newX);
			d3.select('#graph-scale').select('g')
				.transition()
				.duration(600)
				.call(graphAxis);
		});
	}
	window.graphDomain = graphDomain;

	function ungraphDomain() {
		domainSelected = false;
		bubbleWidget.selectReferer('');
		d3.select('.aui-sidebar').classed('domain', false);
		d3.select('#dotz').classed('domain', false);

		monthSvgs.data(generalData['months']).each(function(monthData, i) {
			var lastX = x(monthData.d[monthData.d.length - 1]);
			var shiftedX = function(d) { return x(d) - lastX };
			d3.select(this).selectAll('path')
				.transition()
				.duration(500)
				.delay(i * 30)
				.attr('transform', 'translate(' + lastX + ',0)');

			d3.select('#icon')
				.selectAll('image')
				.transition().duration(1000)
				.attr('transform', 'translate(20,20)')
				.remove();

			graphAxis.scale(x);
			d3.select('#graph-scale').select('g')
				.transition()
				.duration(600)
				.call(graphAxis);
		});
	}
	window.ungraphDomain = ungraphDomain;

	//function 

});

