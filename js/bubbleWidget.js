(function() {
	Traffic.BubbleWidget = function(container, selectCallback) {
		this.tbody = container.select('tbody');
		this.svg   = container.select('svg');
		this.size = this.svg.node().getBoundingClientRect();

		this.svg.style('height', this.size.width + 'px');
		this.size = this.svg.node().getBoundingClientRect();

		this.nodes = [];
		this.force = d3.layout.force()
			.nodes(this.nodes)
			.size([this.size.width, this.size.height])
			.gravity(0.05)
			.charge(0);
		this.pie = d3.layout.pie()
			.sort(null)
			.padAngle(.02)
			.value(function(d) { return d[1] });

		this.selectCallback = selectCallback;
	}

	function getRefererFavicon(referer) {
		return 'http://' + referer[0] + '/favicon.ico';
	}
	function setRefererHighlight(referer, makeHighlighted) {
		d3.select(referer[1]['tableRow']).classed('highlight', makeHighlighted);
		d3.select(referer[1]['bubble'  ]).classed('highlight', makeHighlighted);
	}
	function setPageHighlight(page, makeHighlighted) {
		d3.select(page['tableRow']).classed('highlight', makeHighlighted);
		d3.select(page['pieArc'  ]).classed('highlight', makeHighlighted);
	}
	function collide(alpha, quadtree) {
		return function(d) {
			var r = d.radius + 12,
				nx1 = d.x - r,
				nx2 = d.x + r,
				ny1 = d.y - r,
				ny2 = d.y + r;
			quadtree.visit(function(quad, x1, y1, x2, y2) {
				if (quad.point && (quad.point !== d)) {
					var x = d.x - quad.point.x,
						y = d.y - quad.point.y,
						dist = Math.sqrt(x * x + y * y),
						r = d.radius + quad.point.radius;
					if (dist < r) {
						dist = (dist - r) / dist * alpha;
						d.x -= x *= dist;
						d.y -= y *= dist;
						if (d.radius > 0) {
							quad.point.x += x;
							quad.point.y += y;
						}
					}
				}
				return x1 > nx2 || x2 < nx1 || y1 > ny2 || y2 < ny1;
			});
		};
	}
	Traffic.BubbleWidget.prototype.update = function(data) {
		var that = this;
		var rows = this.tbody.selectAll('tr').data(data, function(d) { return d[0] });

		// Old dogs
		rows.exit().remove().each(function(referer) {
			referer[1]['forceNode'].targetRadius = 0;
		});

		// New boyz
		var newRows = rows.enter().append('tr')
			.attr('class', 'not-page')
			.on('mouseover', function(r) { setRefererHighlight(r, true ) })
			.on('mouseout',  function(r) { setRefererHighlight(r, false) })
			.on('click',     function(r) { that.selectCallback(r[0]) });
		var firstColumns = newRows.append('td');
		firstColumns.append('img').attr('src', getRefererFavicon);
		firstColumns.append('span').text(function(d) { return d[0] });
		newRows.append('td');

		// Everybody now, update nodes (modify in place to preserve physics)
		rows.each(function(referer) {
			var node = that.nodes.find(function(n) { return n.referer[0] == referer[0] });
			if (!node) { // Didn't find anybody :( Need to make a friend D;
				var distance = 300;
				var angle = Math.random() * Math.PI * 2;
				node = {
					radius: 0,
					x: (Math.cos(angle) * distance) + that.size.width  / 2,
					y: (Math.sin(angle) * distance) + that.size.height / 2
				};
				that.nodes.push(node);
			}
			node.referer = referer;
			node.targetRadius = Math.sqrt(referer[1]['c'] * .1);
			referer[1]['forceNode'] = node;
		});

		rows.order().each(function(d) { d[1]['tableRow'] = this });
		rows.select('td:first-child').each(function(r) {
			var tails = d3.select(this).selectAll('.tail')
				.data(
					function(r) { return r[1]['p'].slice(0, 5) }, // First 5 pages
					function(r) { return r[0] }
				);
			tails.exit().remove();
			tails.enter().append('a')
				.attr('class', 'tail')
				.attr('target', '_blank')
				.attr('href', function(p) {
					var a = p[0].split('@');
					return a[0] + r[0] + a[1];
				}).text(function(p) {
					var s = p[0].split('@')[1];
					return s.length > 0 ? s : '/';
				});
			tails.order();
		});
		rows.select('td:last-child').text(function(r) {
			return Traffic.thousands(r[1]['c'])
		});

		// [ All the function stuff was here ]

		var circleGs = this.svg.selectAll('g').data(
			this.nodes,
			function(node) { return node.referer[0] }
		);
		var newCircleGs = circleGs.enter().append('g')
			.call(this.force.drag)
			.on('mouseover', function(n) { setRefererHighlight(n.referer, true ) })
			.on('mouseout',  function(n) { setRefererHighlight(n.referer, false) })
			.on('click',     function(n) {
				if (!d3.event.defaultPrevented) that.selectCallback(n.referer[0]);
			});
		newCircleGs.append('circle');
		newCircleGs.append('image')
			.attr('xlink:href', function(node) { return getRefererFavicon(node.referer) })
			.attr('width',  '16')
			.attr('height', '16')
			.attr('transform', 'scale(0)');
		circleGs.each(function(node) { node.referer[1]['bubble'] = this });
		circleGs.select('circle')
			.attr('r', function(node) { return node.radius })
			.transition().duration(1000)
			.attrTween('r', function(node) {
				var lerp = d3.interpolate(node.radius, node.targetRadius);
				return function(t) { return node.radius = lerp(t); };
			})
			// Bubbles animating to 0 aim to disappear
			.filter(function(node) { return node.targetRadius === 0 })
			.each('end', function(node) {
				d3.select(node.referer[1]['bubble']).remove();
				for (var i = 0; i < that.nodes.length; i++) {
					if (that.nodes[i] === node.referer[1]['forceNode']) {
						that.nodes.splice(i, 1);
						break;
					}
				}
				that.force.start();
			});
		circleGs.select('image')
			.transition().duration(1000)
			.attr('transform', function(node) {
				return (node.targetRadius > 12) ? 'translate(-8,-8)' : 'scale(0)';
			});

		this.force.start();
		this.force.on('tick', function(e) {
			var quadtree = d3.geom.quadtree(that.nodes);
			circleGs.each(collide(.5, quadtree))
				.attr('transform', function(node) { return 'translate(' + node.x + ',' + node.y + ')' });
		});
	};
	Traffic.BubbleWidget.prototype.selectReferer = function(refererName) {

		this.tbody.selectAll('.selected').classed('selected', false);
		this.tbody.selectAll('.page').remove();

		this.svg.selectAll('.selected')
			.classed('selected', false)
			.selectAll('.page-arc').remove();

		var selectedRow = this.tbody.selectAll('tr').filter(function(r) {
			return r[0] == refererName;
		});

		if (!selectedRow.empty()) {
			var referer = selectedRow.datum();
			//var clickedRow    = d3.select(referer[1]['tableRow']);
			//var clickedBubble = d3.select(referer[1]['bubble'  ]);
			var selectedBubble = d3.select(referer[1]['bubble']);

			var domain = referer[0];

			selectedRow.classed('selected', true);

			var newRows = this.tbody.selectAll('.page')
				.data(referer[1]['p']).enter()
				.insert('tr', '.selected ~ .not-page')
				.attr('class', 'page')
				.each(function(d) { d['tableRow'] = this })
				.on('mouseover', function(d) { setPageHighlight(d, true ) })
				.on('mouseout',  function(d) { setPageHighlight(d, false) })
			newRows.append('td')
				.append('a')
				.attr('target', '_blank')
				.attr('href', function(d) {
					var a = d[0].split('@');
					return a[0] + domain + a[1];
				})
				.html(function(d) {
					var a = d[0].split('@');
					return a[0] + '<span>' + domain + '</span>' + a[1];
				});
			newRows.append('td').text(function(d) { return Traffic.thousands(d[1]) });

			var pie = d3.layout.pie()
				.sort(null)
				.value(function(d) { return d[1] });
			var radius = referer[1]['forceNode'].targetRadius;
			var arc = d3.svg.arc()
				.outerRadius(radius)
				.innerRadius(radius > 12 ? 12 : 0);

			// Move to back so white outlines don't cover any other outlines
			//selectedBubble.node().parentNode.insertBefore(selectedBubble.node(), selectedBubble.node().parentNode.firstChild);
			selectedBubble.classed('selected', true);
			selectedBubble.selectAll('.page-arc')
				.data(pie(referer[1]['p']))
				.enter().append('path')
					.attr('d', arc)
					.attr('class', 'page-arc')
					.each(function(d) {
						d.data['pieArc'] = this;
					})
					.on('mouseover', function(d) { setPageHighlight(d.data, true ) })
					.on('mouseout',  function(d) { setPageHighlight(d.data, false) });
		}
	}
})();
